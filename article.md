title: "Víc mandátů pro velké hráče, trest za rozdrobené síly. Co způsobuje D'Hondtova metoda přepočtu hlasů ve volbách"
perex: "Rozdělením křesel v nové sněmovně zamíchaly dvě malé strany - TOP 09 a Starostové a nezávislí. Obě dostaly jen lehce přes pět procent hlasů, tedy počet, který musí překročit, aby se mohly ucházet o mandáty. Součet jejich hlasů je podobný jako ve volbách v roce 2013, kdy kandidovaly společně. Mandátů ale mají dohromady jen polovinu."
authors: ["Michal Zlatkovský"]
published: "27. září 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/images/02815614.jpeg
coverimg_note: ""
styles: ["https://interaktivni.rozhlas.cz/data/dhondtator/jquery-ui.min.css","//cdn.datatables.net/1.10.16/css/jquery.dataTables.css","https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css",""]
libraries: ["https://unpkg.com/jquery@3.2.1", "https://interaktivni.rozhlas.cz/data/dhondtator/jquery-ui.min.js", "https://cdn.jsdelivr.net/npm/lodash@4.17.4/lodash.min.js","//cdn.datatables.net/1.10.16/js/jquery.dataTables.js","https://cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"]
options: "" #wide
---

Je to dáno takzvanou D'Hondtovou metodou pro přepočet hlasů na mandáty, která "nahrává" stranám s velkým počtem hlasů. V českých volbách do Poslanecké sněmovny se používá od roku 2002, poté, co ji v době [opoziční smlouvy](https://www.irozhlas.cz/veda-technologie/historie/jak-si-zeman-s-klausem-rozdelili-cesko_1710090800_zlo) společně zavedla ODS a ČSSD. Zjednodušeně řečeno funguje tak, že jednotlivé volební obvody - v Česku jich je dnes 14 a jsou totožné s kraji - si na základě počtu odevzdaných hlasů rozdělí 200 sněmovních mandátů. V každém kraji se pak přidělují mandáty zvlášť [postupným dělením počtu hlasů pro jednotlivé strany](http://zpravy.idnes.cz/vypocet-zisku-mandatu-a-penez-za-volby-dyi-/domaci.aspx?c=A130910_153416_domaci_jw). Používá se kvůli snadnějšímu sestavování vládních koalicí.

Praktický důsledek D'Hondtova přepočtu je patrný v proběhlých sněmovních volbách. Zatímco jejich vítěz, hnutí ANO, potřebovalo k zisku jednoho mandátu průměrně 19 tisíc hlasů, u nejmenší úspěšné strany STAN to bylo více než dvakrát tolik. Malé strany tak doplatily na svou rozdrobenost.

<table id="hlasy"></table><br>

Vítězi voleb zejména na úkor malých stran nahrává i nerovnoměrné rozdělení na volební obvody. Jeho důsledkem je, že krajům s malým počtem voličů - například Karlovarskému a Libereckému - připadne mnohem méně mandátů k rozdělení než třeba Praze. Ty pak kvůli D'Hondtovu přepočtu připadnou jen několika málo vítězům a na zbytek stran se nedostane. Výrazným příkladem je Strana zelených ve volbách v roce 2006. V Libereckém kraji získala 9,58 % hlasů, ale nepřipadl ji jediný z osmi mandátů.

<table id="kraje"></table><br>

Funkci D'Hondtovy metody ještě posiluje takzvaná uzavírací klauzule, která je jednou z klíčových součástí [poměrného volebního systému](https://cs.wikipedia.org/wiki/Pom%C4%9Brn%C3%BD_volebn%C3%AD_syst%C3%A9m) a je běžná ve většině evropských zemí. Zavádí se jako pojistka proti přílišnému tříštění parlamentu, které by ztěžovalo následný vznik vlády. "Příliš velké množství politických stran zastoupených v parlamentu zpravidla vznik a fungování stabilních vlád komplikuje," říká pro server iROZHLAS.cz politolog Vlastimil Havlík z Masarykovy univerzity. "Jak ukazují studie například ze západní Evropy, vlády s větším počtem stran mají nižší trvanlivost."

Extrémním případem rozdrobení je Polsko, kde po volbách bez klauzule v roce 1991 zasedlo do parlamentu celkem 29 stran. V Česku existuje pětiprocentní klauzule už od prvních svobodných voleb v roce 1990 a podle Havlíka v evropském srovnání zásadně nevybočuje. Jak ovšem ukazují aktuální volební výsledky, ani ona není samospásná - do Poslanecké sněmovny zasedlo rekordních devět stran. Ve světě najdeme i výrazně nižší (Nizozemsko, 0,67 procenta) nebo vyšší (Turecko, 10 procent) hranice. 

V naší kalkulačce si můžete spočítat, jak by se změnil poměr stranických sil v Poslanecké sněmovně při zvýšení, snížení nebo úplném zrušení pětiprocentní hranice. Použili jsme reálná data z aktuálních sněmovních voleb spolu s historickými výsledky z let 2006, 2010 a 2013. 

Jak podotýká Vlastimil Havlík, na takový model je ale potřeba pohlížet se zdravou mírou skepse. Dokáže totiž zachytit jen mechanické působení volebního systému, ne psychologické účinky klauzule. "Pokud by klauzule neexistovala, hlasy pro malé strany nepropadají a volič nemá důvod přemýšlet strategicky," vysvětluje Havlík. Místo zvažování, která strana má šanci vstoupit do sněmovny, by voliči rovnou hlasovali pro své favority.

<b>Nastavte uzavírací klauzuli:</b>
<div id="slider">
  <div id="custom-handle" class="ui-slider-handle"></div>
</div>
<b>Zvolte volební rok:</b><br>
<input type="radio" name="rok" id="rok2006" value="2006"><label class="inlabel" for="rok2006">2006</label><br>
<input type="radio" name="rok" id="rok2010" value="2010"><label class="inlabel" for="rok2010">2010</label><br>
<input type="radio" name="rok" id="rok2013" value="2013"><label class="inlabel" for="rok2013">2013</label><br>
<input type="radio" name="rok" id="rok2017" value="2017" checked><label class="inlabel" for="rok2017">2017</label><br>
<table id="vysledky" style="margin-top: 1.5em;">
    <tbody style="border-top:0">
    </tbody>
</table>
<i>* Pokud kalkulačka přidělila straně 0 mandátů, znamená to, že strana sice prošla přes nastavenou hranici, ale volební přepočet podle d'Hondtovy metody (viz níže) jí nepřidělil žádný mandát.</i>
	
Kromě mechanického a psychologického efektu má klauzule i další účinky. Ty podle Havlíka závisí na tom, jak se voliči učí pravidlům volebního systému, tedy mechanickému účinku, na společenském kontextu, na typu voleb - v méně důležitých volbách je psychologický účinek menší -, ale také na nastavení financování politických stran. Ty mohou například argumentovat, že je důležité překonat aspoň hranici hlasů, která jim zajistí finance od státu, v Česku tedy od 1,5 % výše.

Modelováním volebních výsledků za použití různých pravidel [se pravidelně věnují](https://www.kosmas.cz/knihy/202483/volby-do-poslanecke-snemovny-2013/) i výzkumníci Masarykovy univerzity, mezi které Vlastimil Havlík patří. Kromě hypotetického zrušení klauzule ukazují i výsledky voleb při přejití od d'Hondtovy metody k některému z konkurenčních způsobů přepočtu nebo při změně volebních obvodů. "Změna volebního systému nadále nabízí rychlé a jednoduché řešení, byť je samozejmě otázkou, o jakou reformu by se mělo jednat a jestli by skutečně vedla ke kýženému cíli," píšou ve své práci.