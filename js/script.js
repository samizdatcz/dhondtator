// responsive slider bazmek
!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);




$(function() {

    krajtab = [
        ["ANO", "29,64 %", "78", "64"],
        ["ODS", "11,32 %", "25", "24"],
        ["Piráti", "10,79 %", "22", "23"],
        ["SPD", "10,64 %", "22", "23"],
        ["KSČM", "7,76 %", "15", "17"],
        ["ČSSD", "7,27 %", "15", "15"],
        ["KDU-ČSL", "5,80 %", "10", "12"],
        ["TOP 09", "5,31 %", "6", "11"],
        ["STAN", "5,18 %", "6", "11"],
    ]

    $('#kraje').DataTable( {
        language: {url: "https://interaktivni.rozhlas.cz/tools/datatables/Czech.json" },
        responsive: true,
        data: krajtab,
        order: [[1, 'desc']],
        searching: false,
        lengthChange: false,
        paging: false,
        info: false,        
        columns: [
            { title: "Strana", responsivePriority: 1 },
            { title: "Procent" },
            { title: "Mandátů (skutečně)" },
            { title: "Mandátů (jeden volební kraj)", responsivePriority: 2 },
        ]
    } );    

    hlastab = [
        ["ANO", "1500113", "78", "19232"],
        ["ODS", "572962", "25", "22918"],
        ["Piráti", "546393", "22", "24836"],
        ["SPD", "538574", "22", "24481"],
        ["KSČM", "393100", "15", "26207"],
        ["ČSSD", "368347", "15", "24556"],
        ["KDU-ČSL", "293643", "10", "29364"],
        ["TOP 09", "268811", "6", "38402"],
        ["STAN", "262157", "6", "43693"],
    ]

    $('#hlasy').DataTable( {
        language: {url: "https://interaktivni.rozhlas.cz/tools/datatables/Czech.json" },
        responsive: true,
        data: hlastab,
        order: [[1, 'desc']],
        searching: false,
        lengthChange: false,
        paging: false,
        info: false,
        columns: [
            { title: "Strana", responsivePriority: 1 },
            { title: "Hlasů" },
            { title: "Mandátů" },
            { title: "Hlasů na mandát", responsivePriority: 2 },
        ]
    } );    

    hranice = 5; // hranice pro vstup do snemovny
    rok = 2017; // ktera data  

    // defaultni zobrazeno
    getDhondt()

    // selektitko kvora
    var handle = $( "#custom-handle" );
    $( "#slider" ).slider({
      min: 0,
      max: 10,
      step: 0.1,
      value: 5,
      create: function() {
        handle.text( $( this ).slider( "value" ) + " %" );
      },
      slide: function( event, ui ) {
        handle.text( _.replace(ui.value,".",",") + " %");
        hranice = ui.value;
        getDhondt()
      }
    });

    // selektitko roku
    $("input[name=rok]").click(function() {
        rok = $(this).val();
        getDhondt()
    })   

    // ajax retrieval dat
    function getDhondt() {
        $.ajax({
            type: "GET" ,
            // https://interaktivni.rozhlas.cz/data/dhondtator/data/vysledky
            url: "https://interaktivni.rozhlas.cz/data/dhondtator/data/vysledky"+rok+".xml" ,
            dataType: "xml" ,
            success: function(xml) {
                xxml = $(xml);
                dhondt(xxml,hranice)
            }
        });  
    };

    // hlavni funkce
    function dhondt(xml, hranice) {
        $xml = xml
        vysledky = [];

        // skrutinium + pct pro zobrazeni vysledku
        vysledekPct = {};
        strany = [];
        $.each($xml.find("CR").find("STRANA"), function (id,strana) {
              pct = $(strana).find("HODNOTY_STRANA").attr("PROC_HLASU")
              vysledekPct[$(strana).attr("NAZ_STR")] = pct
              if (pct > parseFloat(hranice)) {
                   strany.push($(strana).attr("KSTRANA"))
              };
        });

        $.each($xml.find("KRAJ"), function(id,kraj) {
              nazev = $(kraj).attr("NAZ_KRAJ");
              hlasy = $(kraj).find("UCAST").attr("PLATNE_HLASY");

              vyslstrany = [];

              $.each($(kraj).find("STRANA"), function(id,strana) {
                if (_.includes(strany, $(strana).attr("KSTRANA"))) {
                    vyslstrany.push([$(strana).attr("NAZ_STR"), $(strana).find("HODNOTY_STRANA").attr("HLASY"), 0])
                }
              });

              vysledky.push([nazev, hlasy, vyslstrany])
        });

        // republikove mandatove cislo
        hlasyCelkem = 0;
        $.each($(vysledky), function(id,kraj) {
            hlasyCelkem+=parseInt(kraj[1]);
        });
        rmc = Math.round(hlasyCelkem/200);

        // vypocet zakladniho poctu mandatu v jednotlivych krajich
        mtotal = 0;
        $.each($(vysledky), function(id,kraj) {
            mbase = kraj[1]/rmc;
            kraj.push(Math.floor(mbase/1), mbase % 1)
            mtotal+=parseInt(mbase)
        });        

        // dopocet do 200 mandatu
        mmissing = 200 - mtotal
        
        vysledky.sort(function(a,b){return parseFloat(b[4]) - parseFloat(a[4])})

        for (var i = 0; i < mmissing; i++) {
            vysledky[i][3] += 1;
        }

        // dhondtuv prepocet
        $.each($(vysledky), function(id,kraj) {

            // deli se jednickou az poctem mandatu
            mandaty = kraj[3]
            dhondtlist = []

            // vypocteni podilu
            $.each($(kraj[2]), function (id, strana) {
                for (var i = 0; i < mandaty; i++) {
                    dhondtlist.push([strana[0], Math.round(strana[1]/(i+1))])
                }                
            });


            //rozdeleni mandatu
            dhondtlist.sort(function(a,b){return parseFloat(b[1]) - parseFloat(a[1])})
            dhondtlist = _.slice(dhondtlist,0,mandaty)

            $.each($(dhondtlist), function (id, mandat) {
                $.each($(kraj[2]), function (id, strana) {
                    if (mandat[0] == strana[0]) {
                        strana[2] += 1
                    }
                });
            });
        });        
        
        //soucet
        soucet = {};

        $.each($(vysledky), function (id, kraj) {
            $.each($(kraj[2]), function (id, strana) {

                if (soucet[strana[0]]) {    
                    soucet[strana[0]] += strana[2]
                }
                else {
                    soucet[strana[0]] = strana[2]
                }
            });              
        });

        vysledek = [];

        lidskeNazvy = [
            ["Česká str.sociálně demokrat.", "ČSSD"],
            ["ANO 2011", "ANO"],
            ["Občanská demokratická strana", "ODS"],
            ["Křesť.demokr.unie-Čs.str.lid.", "KDU-ČSL"],
            ["Komunistická str.Čech a Moravy", "KSČM"],
            ["Úsvit přímé demokr.T.Okamury", "Úsvit"],
            ["Strana Práv Občanů ZEMANOVCI", "SPOZ"],
            ["Dělnic.str.sociální spravedl.", "DSSS"],
            ["Česká pirátská strana", "Piráti"],
            ["Strana svobodných občanů", "Svobodní"],
            ["Suverenita-blok J.Bobošíkové", "Suverenita"],
            ["NEZ.DEMOKRATÉ(předs.V.Železný)", "Nezávislí demokraté"],
            ["Svob.a př.dem.-T.Okamura (SPD)", "SPD"]
        ];

        for (zaznam in soucet) {

            //zlidsteni nazvu
            lidskyNazev = zaznam

            $.each($(lidskeNazvy), function(id, nazev) {
                if (lidskyNazev != _.replace(lidskyNazev, nazev[0], nazev[1])) {
                    lidskyNazev = _.replace(lidskyNazev, nazev[0], nazev[1])
                }
            });

            vysledek.push([lidskyNazev, soucet[zaznam], vysledekPct[zaznam]])
        }

        vysledek.sort(function(a,b){return (b[2] - a[2])})

        //vypis
        $('#vysledky tbody').empty().append("<tr><td><b>Strana</b></td><td><b>Procent</b></td><td><b>Mandátů</b></td></tr>")
        $.each(vysledek, function (id, radek) {
            $('#vysledky tbody').append("<tr><td class='vysltd'>" + radek[0] + "</td><td class='vysltd'>" + _.replace(radek[2],".",",") + " %</td><td class='vysltd'>" + _.replace(radek[1],/^0/,"0*") + "</td></tr>")
        });

    }; 

});